package up42.demo.exception;

import lombok.Getter;

@Getter
public class ResourceNotFoundException extends RuntimeException{
    private String message;

    public static ResourceNotFoundException createWith(String message) {
        return new ResourceNotFoundException(message);
    }

    private ResourceNotFoundException(String message) {
        this.message = message;
    }
}
