package up42.demo.feature.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import up42.demo.feature.common.FeatureLoader;
import up42.demo.feature.domain.Feature;
import up42.demo.feature.port.out.FeatureRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FeatureLoaderRepository implements FeatureRepository {

    private static Map<String, Feature> featureMap = FeatureLoader.loadFeatures();

    public List<Feature> loadAllFeatures(){
        return new ArrayList<>(featureMap.values());
    }

    public Optional<Feature> findFeatureById(String id){
        return Optional.ofNullable(featureMap.get(id));
    }
}
