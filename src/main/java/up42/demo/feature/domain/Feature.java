package up42.demo.feature.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Feature {

    private String id;
    private Long timestamp;
    private Long beginViewingDate;
    private Long endViewingDate;
    private String missionName;
    private String image;
}
