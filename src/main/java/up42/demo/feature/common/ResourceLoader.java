package up42.demo.feature.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import up42.demo.exception.ResourceNotFoundException;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
public class ResourceLoader {

    private static final String RESOURCE_LOCATION = "source-data.json";

    public static String loadResourceAsString() {
        try{
            Resource resource = new ClassPathResource(RESOURCE_LOCATION);
            InputStream input = resource.getInputStream();
            return new String(input.readAllBytes(), StandardCharsets.UTF_8);
        }catch (Exception e){
            String message = String.format("Resource %s not found ", RESOURCE_LOCATION);
            log.error(message);
            throw ResourceNotFoundException.createWith(message);
        }
    }
}
