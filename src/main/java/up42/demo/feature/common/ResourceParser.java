package up42.demo.feature.common;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import up42.demo.feature.domain.Feature;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static up42.demo.feature.common.FeatureFactory.buildFeature;

public class ResourceParser{

    public static Map<String, Feature> parseResource(String json) {
        JsonArray jsonElements = JsonParser.parseString(json).getAsJsonArray();

        return StreamSupport.stream(jsonElements.spliterator(), false)
                .flatMap(a -> StreamSupport.stream(a.getAsJsonObject().get(FeatureConstants.FEATURES_JSON_FIELD).getAsJsonArray().spliterator(), false))
                .map(b -> buildFeature(b.getAsJsonObject().get(FeatureConstants.PROPERTIES_JSON_FIELD).getAsJsonObject()))
                .collect(Collectors.toConcurrentMap(Feature::getId, Function.identity()));
    }
}
