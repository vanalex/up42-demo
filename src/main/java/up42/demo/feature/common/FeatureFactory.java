package up42.demo.feature.common;

import com.google.gson.JsonObject;
import up42.demo.feature.domain.Feature;

import java.util.Objects;

public class FeatureFactory {

    private FeatureFactory(){}

    public static Feature buildFeature(JsonObject properties) {
        JsonObject acquisition = properties.get(FeatureConstants.ACQUISITION_JSON_FIELD).getAsJsonObject();
        return Feature.builder()
                .id(properties.get(FeatureConstants.ID_JSON_FIELD).getAsString())
                .timestamp(properties.get(FeatureConstants.TIMESTAMP_JSON_FIELD).getAsLong())
                .beginViewingDate(acquisition.get(FeatureConstants.BEGIN_VIEWING_DATE_JSON_FIELD).getAsLong())
                .endViewingDate(acquisition.get(FeatureConstants.END_VIEWING_DATE_JSON_FIELD).getAsLong())
                .missionName(acquisition.get(FeatureConstants.MISSION_NAME_JSON_FIELD).getAsString())
                .image(Objects.nonNull(properties.get(FeatureConstants.QUICKLOOK_JSON_FIELD)) ? properties.get(FeatureConstants.QUICKLOOK_JSON_FIELD).getAsString() : null)
                .build();
    }
}
