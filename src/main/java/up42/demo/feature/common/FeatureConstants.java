package up42.demo.feature.common;

public class FeatureConstants {
    public static final String ID_JSON_FIELD = "id";
    public static final String FEATURES_JSON_FIELD = "features";
    public static final String PROPERTIES_JSON_FIELD = "properties";
    public static final String ACQUISITION_JSON_FIELD = "acquisition";
    public static final String TIMESTAMP_JSON_FIELD = "timestamp";
    public static final String BEGIN_VIEWING_DATE_JSON_FIELD = "beginViewingDate";
    public static final String END_VIEWING_DATE_JSON_FIELD = "endViewingDate";
    public static final String MISSION_NAME_JSON_FIELD = "missionName";
    public static final String QUICKLOOK_JSON_FIELD = "quicklook";
}
