package up42.demo.feature.common;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import up42.demo.feature.domain.Feature;

import java.util.Map;

import static up42.demo.feature.common.ResourceParser.parseResource;

@Component
@Slf4j
@Getter
public class FeatureLoader{
    private static Map<String, Feature> featureMap;

    static{
        featureMap = loadFeatures();
    }

    public static Map<String, Feature> loadFeatures() {
        return parseResource(ResourceLoader.loadResourceAsString());
    }
}
