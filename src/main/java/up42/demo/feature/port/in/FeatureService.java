package up42.demo.feature.port.in;

import up42.demo.feature.domain.Feature;

import java.util.List;

public interface FeatureService {

    List<Feature> loadAllFeatures();

    Feature loadFeatureById(String id);

    byte[] loadImageFeatureById(String id);
}
