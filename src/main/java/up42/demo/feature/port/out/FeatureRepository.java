package up42.demo.feature.port.out;

import up42.demo.feature.domain.Feature;

import java.util.List;
import java.util.Optional;

public interface FeatureRepository {

    List<Feature> loadAllFeatures();
    Optional<Feature> findFeatureById(String id);
}
