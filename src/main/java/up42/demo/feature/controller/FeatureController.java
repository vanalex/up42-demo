package up42.demo.feature.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import up42.demo.feature.domain.Feature;
import up42.demo.feature.port.in.FeatureService;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class FeatureController {

    private final FeatureService featureService;

    @GetMapping("/features")
    public ResponseEntity<List<Feature>> loadFeatures(){
        return ResponseEntity.ok(featureService.loadAllFeatures());
    }

    @GetMapping(value = "/features/{id}")
    public ResponseEntity<Feature> loadFeature(@NotNull @PathVariable("id") String id){
        return ResponseEntity.ok(featureService.loadFeatureById(id));
    }

    @GetMapping(value = "/features/{id}/quicklook", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> loadImage(@PathVariable("id") @NotNull String id){
        return ResponseEntity.ok(featureService.loadImageFeatureById(id));
    }
}
