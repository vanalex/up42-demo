package up42.demo.feature.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import up42.demo.exception.ResourceNotFoundException;
import up42.demo.feature.domain.Feature;
import up42.demo.feature.port.in.FeatureService;
import up42.demo.feature.port.out.FeatureRepository;

import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FeatureServiceImpl implements FeatureService {

    private final FeatureRepository featureRepository;

    public List<Feature> loadAllFeatures(){
        return featureRepository.loadAllFeatures();
    }

    public Feature loadFeatureById(String id){
        return fetchFeatureById(id);
    }

    public byte[] loadImageFeatureById(String id){
        Feature feature = fetchFeatureById(id);
        return Base64.getDecoder().decode(feature.getImage().getBytes());
    }

    private Feature fetchFeatureById(String id){
        return featureRepository.findFeatureById(id).orElseThrow(() -> ResourceNotFoundException.createWith(String.format("Resource not found with id %s", id)));
    }
}
