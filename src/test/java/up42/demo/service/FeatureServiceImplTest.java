package up42.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import up42.demo.Testfixtures;
import up42.demo.exception.ResourceNotFoundException;
import up42.demo.feature.domain.Feature;
import up42.demo.feature.port.out.FeatureRepository;
import up42.demo.feature.service.FeatureServiceImpl;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FeatureServiceImplTest {

    @Mock
    private FeatureRepository featureLoaderRepository;

    @InjectMocks
    private FeatureServiceImpl featureService;

    @Test
    public void testLoadAllFeatures(){
        when(featureLoaderRepository.loadAllFeatures()).thenReturn(Testfixtures.getFeatureList());

        List<Feature> result = featureService.loadAllFeatures();

        assertThat(result).isNotEmpty();
        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    public void testLoadFeatureById(){
        String id = "1ed0b505-2c50-4420-855c-a36d04c55661";
        when(featureLoaderRepository.findFeatureById(Mockito.anyString())).thenReturn(Optional.ofNullable(Testfixtures.buildFeature()));

        Feature result = featureService.loadFeatureById(id);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo("1ed0b505-2c50-4420-855c-a36d04c55661");
        assertThat(result.getTimestamp()).isEqualTo(1555477219509L);
        assertThat(result.getBeginViewingDate()).isEqualTo(1555477219509L);
        assertThat(result.getEndViewingDate()).isEqualTo(1555477244507L);
        assertThat(result.getMissionName()).isEqualTo("Sentinel-1B");
    }

    @Test
    public void testLoadFeatureByIdWhenId(){
        when(featureLoaderRepository.findFeatureById(Mockito.anyString())).thenThrow(ResourceNotFoundException.createWith("Resource not found"));

        assertThatThrownBy(() -> featureService.loadFeatureById("1"))
                .isInstanceOf(ResourceNotFoundException.class)
                .hasMessage("Resource not found");
    }

}
