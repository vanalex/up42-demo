package up42.demo.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import up42.demo.exception.ResourceNotFoundException;
import up42.demo.feature.domain.Feature;
import up42.demo.feature.port.in.FeatureService;
import up42.demo.feature.service.FeatureServiceImpl;

import java.util.List;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
class FeatureServiceImplIntegrationTest {

    private FeatureService featureService;

    @Autowired
    public FeatureServiceImplIntegrationTest(FeatureServiceImpl featureServiceImpl) {
        this.featureService = featureServiceImpl;
    }

    @Test
    void testGivenAFeatureIdWhenLoadFeatureByIdThenFeatureShouldBeLoaded(){
        Feature feature = featureService.loadFeatureById("12d0b505-2c70-4420-855c-936d05c55669");
        assertThat(feature).isNotNull();
        assertThat(feature.getId()).isEqualTo("12d0b505-2c70-4420-855c-936d05c55669");
        assertThat(feature.getTimestamp()).isEqualTo(1555477219508L);
        assertThat(feature.getBeginViewingDate()).isEqualTo(1555477219508L);
        assertThat(feature.getEndViewingDate()).isEqualTo(1555477244506L);
        assertThat(feature.getMissionName()).isEqualTo("Sentinel-1A");
    }

    @ParameterizedTest
    @ValueSource(strings = {"12d0b505-2c70-a420-855c-136d05c55660"})
    void testGivenWrongFeatureIdWhenLoadFeatureByIdThenExceptionShouldBeThrown(String value) {
        assertThatThrownBy(() -> featureService.loadFeatureById(value))
                .isInstanceOf(ResourceNotFoundException.class)
        .hasMessage("Resource not found with id 12d0b505-2c70-a420-855c-136d05c55660");
    }

    @Test
    void testWhenLoadFeaturesThenFeaturesShouldBeLoaded(){
        List<Feature> features = featureService.loadAllFeatures();
        assertThat(features.size()).isEqualTo(14);
    }

    @Test
    void testGivenFeatureIdWhenLoadingIaageByFeatureIdThenImageShouldBeReturn(){
        byte[] image = featureService.loadImageFeatureById("12d0b505-2c70-4420-855c-936d05c55669");
        assertThat(image).isNotNull();
        assertThat(image).isExactlyInstanceOf(byte[].class);
    }
}
