package up42.demo.common;

import org.junit.jupiter.api.Test;
import up42.demo.feature.common.ResourceLoader;
import up42.demo.feature.common.ResourceParser;
import up42.demo.feature.domain.Feature;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ResourceParserTest {

    @Test
    void testGivenResourceAsJsonWhenParseThenMapShouldBeGivenAsAResult(){
        String json = ResourceLoader.loadResourceAsString();
        Map<String, Feature> result = ResourceParser.parseResource(json);
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(14);
    }
}
