package up42.demo.common;

import org.junit.jupiter.api.Test;
import up42.demo.feature.common.ResourceLoader;

import static org.assertj.core.api.Assertions.assertThat;

class ResourceLoaderTest {

    @Test
    void testLoadResourceAsString(){
        String result = ResourceLoader.loadResourceAsString();
        assertThat(result).isNotBlank();
    }
}
