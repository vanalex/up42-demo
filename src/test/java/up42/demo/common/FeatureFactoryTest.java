package up42.demo.common;

import com.google.gson.JsonObject;
import org.junit.Test;
import up42.demo.Testfixtures;
import up42.demo.feature.common.FeatureConstants;
import up42.demo.feature.common.FeatureFactory;
import up42.demo.feature.domain.Feature;

import static org.assertj.core.api.Assertions.assertThat;

public class FeatureFactoryTest {

    @Test
    public void testBuildFeatureGivenJsonProperties(){

        Feature result = FeatureFactory.buildFeature(Testfixtures.buildJsonProperties());

        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo("12d0b505-2c70-4420-855c-936d05c55669");
        assertThat(result.getTimestamp()).isEqualTo(1555477219509L);
        assertThat(result.getBeginViewingDate()).isEqualTo(1555477219509L);
        assertThat(result.getEndViewingDate()).isEqualTo(1555477219509L);
        assertThat(result.getMissionName()).isEqualTo("Sentinel-1B");

    }
}
