package up42.demo;

import com.google.gson.JsonObject;
import up42.demo.feature.common.FeatureConstants;
import up42.demo.feature.domain.Feature;

import java.util.List;

public class Testfixtures {

    public static List<Feature> getFeatureList(){
        return List.of(Feature.builder()
                .id("12d0b505-2c70-4420-855c-936d05c55669")
                .timestamp(1555477219508L)
                .beginViewingDate(1555477219508L)
                .endViewingDate(1555477244506L)
                .missionName("Sentinel-1A")
                .build(),
                Feature.builder()
                        .id("1ed0b505-2c50-4420-855c-a36d04c55661")
                        .timestamp(1555477219509L)
                        .beginViewingDate(1555477219509L)
                        .endViewingDate(1555477244507L)
                        .missionName("Sentinel-1B")
                        .build());
    }

    public static Feature buildFeature(){
        return Feature.builder()
                .id("1ed0b505-2c50-4420-855c-a36d04c55661")
                .timestamp(1555477219509L)
                .beginViewingDate(1555477219509L)
                .endViewingDate(1555477244507L)
                .missionName("Sentinel-1B")
                .build();
    }

    public static JsonObject buildJsonProperties(){
        JsonObject acquisition = new JsonObject();
        acquisition.addProperty(FeatureConstants.BEGIN_VIEWING_DATE_JSON_FIELD, 1555477219509L);
        acquisition.addProperty(FeatureConstants.END_VIEWING_DATE_JSON_FIELD, 1555477219509L);
        acquisition.addProperty(FeatureConstants.MISSION_NAME_JSON_FIELD, "Sentinel-1B");

        JsonObject properties = new JsonObject();
        properties.addProperty(FeatureConstants.ID_JSON_FIELD, "12d0b505-2c70-4420-855c-936d05c55669");
        properties.add(FeatureConstants.ACQUISITION_JSON_FIELD, acquisition);
        properties.addProperty(FeatureConstants.TIMESTAMP_JSON_FIELD, 1555477219509L);
        properties.addProperty(FeatureConstants.QUICKLOOK_JSON_FIELD, 1555477219509L);

        return properties;
    }
}
