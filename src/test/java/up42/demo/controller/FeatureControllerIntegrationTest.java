package up42.demo.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class FeatureControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testLoadAllFeatures() throws Exception {
        this.mockMvc.perform(get("/features")
                .header("Content-Type", "application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void testGivenFeatureIdWhenLoadFeatureByIdThenFeatureShouldBeLoaded() throws Exception {

        mockMvc.perform(get("/features/{id}","12d0b505-2c70-4420-855c-936d05c55669")
                .header("Content-Type", "application/json"))
                .andExpect(status().isOk());
    }

    @ParameterizedTest
    @ValueSource(strings = {"12d0b505-2c70-a420-855c-136d05c55660"})
    void testGivenWrongFeatureIdWhenLoadFeatureByIdThenExceptionShouldBeThrown(String value) throws Exception {

        mockMvc.perform(get("/feature/{id}",value)
                .header("Content-Type", "application/json"))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "  "})
    void testGivenEmptyValueAsFeatureIdWhenLoadFeatureByIdThenExceptionshouldBeThrown(String value) throws Exception {

        mockMvc.perform(get("/features/value")
                .header("Content-Type", "application/json"))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @NullSource
    void testGivenNullAsFeatureIdWhenLoadFeatureByIdThenExceptionshouldBeThrown(String value) throws Exception {

        mockMvc.perform(get("/features/value")
                .header("Content-Type", "application/json"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void testGivenFeatureIdWhenLoadImageThenShouldReturnImage() throws Exception {

        mockMvc.perform(get("/features/{id}/quicklook","12d0b505-2c70-4420-855c-936d05c55669")
                .header("Content-Type", "image/png"))
                .andExpect(status().isOk());
    }
}
