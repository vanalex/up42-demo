package up42.demo.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import up42.demo.feature.domain.Feature;
import up42.demo.feature.port.out.FeatureRepository;
import up42.demo.feature.repository.FeatureLoaderRepository;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class FeatureLoaderRepositoryIntegrationTest {

    private FeatureRepository featureRepository;

    @Autowired
    public FeatureLoaderRepositoryIntegrationTest(FeatureLoaderRepository featureLoaderRepository) {
        this.featureRepository = featureLoaderRepository;
    }

    @Test
    public void test(){
        List<Feature> result = featureRepository.loadAllFeatures();
        assertThat(result.size()).isEqualTo(14);
    }
}
